#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

setup(
    name="othello",
    version="0.0.1",
    entry_points={
        "console_scripts": [
            "othello=othello.main:main",
            "othello-stat=othello.stat_main:main",
        ]
    },
)
