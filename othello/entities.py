#!/usr/bin/env python
# -*- coding: utf-8 -*-

from collections import deque
from copy import deepcopy
from dataclasses import dataclass, field
from enum import Enum
from itertools import product, repeat
from typing import Deque, Dict, Iterable, Optional, Set, Tuple

MAX_Y, MAX_X = 8, 8


class Color(Enum):
    BLACK = 1
    WHITE = -1

    def __neg__(self):
        return Color(-self.value)

    def __str__(self):
        if self == Color.BLACK:
            return "B"
        else:
            return "W"


Cell = Optional[Color]
Position = Tuple[int, int]

START_COLORS = [[None for _ in range(MAX_Y)] for _ in range(MAX_X)]
START_COLORS[3][3] = Color.WHITE
START_COLORS[3][4] = Color.BLACK
START_COLORS[4][3] = Color.BLACK
START_COLORS[4][4] = Color.WHITE
START_CELLS = {
    (y, x): START_COLORS[y][x] for (y, x) in product(range(MAX_Y), range(MAX_X))
}


@dataclass
class PutHistory:
    color: Color
    put_position: Position
    changed_positions: Set[Position]


@dataclass(eq=False)
class Board:
    cells: Dict[Tuple[int, int], Cell] = field(
        default_factory=lambda: deepcopy(START_CELLS)
    )
    histories: Deque[Optional[PutHistory]] = field(default_factory=lambda: deque([]))

    @property
    def is_filled(self) -> bool:
        return None not in self.cells.values()

    def count(self, color: Color) -> int:
        return tuple(self.cells.values()).count(color)

    def rollback(self):
        """
        Boardの情報のみを前の状態に戻す
        Gameの情報は変更しないことに注意
        """
        if len(self.histories) == 0:
            return
        history = self.histories.pop()
        if history is not None:
            self.cells[history.put_position] = None
            neg_color = -history.color
            for pos in history.changed_positions:
                self.cells[pos] = neg_color

    def puttable(self, pos: Position, color: Color):
        if self.get(pos) is not None:
            return False

        positions = self.get_changed_positions(pos, color)
        res = next(positions, None)
        return res is not None

    def get_puttable_positions(self, color: Color) -> Iterable[Position]:
        positions = list(product(range(MAX_Y), range(MAX_X)))
        for pos in positions:
            if self.puttable(pos, color):
                yield pos

    def exists_puttable_position(self, color: Color) -> bool:
        i = self.get_puttable_positions(color)
        if next(i, None) is None:
            return False
        else:
            return True

    @property
    def is_finished(self):
        return not self.exists_puttable_position(
            Color.BLACK
        ) and not self.exists_puttable_position(Color.WHITE)

    def put(self, pos: Position, color: Color) -> Optional[PutHistory]:
        if self.get(pos) is not None:
            self.histories.append(None)
            return None
        if pos[0] < 0 or pos[0] >= MAX_Y or pos[1] < 0 or pos[1] > MAX_X:
            self.histories.append(None)
            return None

        changed_positions = set(self.get_changed_positions(pos, color))

        if not changed_positions:
            self.histories.append(None)
            return None

        history = PutHistory(
            color=color, put_position=pos, changed_positions=changed_positions.copy()
        )
        self.histories.append(history)
        changed_positions.add(pos)
        for pos in changed_positions:
            self.cells[pos] = color
        return history

    def get_changed_positions(self, pos, color):
        # TODO: itertools.chainとかをうまく使って ここのforループを消す
        for f in (
            self._get_changed_positions_along_xaxis_plus,
            self._get_changed_positions_along_xaxis_minus,
            self._get_changed_positions_along_yaxis_plus,
            self._get_changed_positions_along_yaxis_minus,
            self._get_changed_positions_along_positive_diag_plus,
            self._get_changed_positions_along_positive_diag_minus,
            self._get_changed_positions_along_negative_diag_plus,
            self._get_changed_positions_along_negative_diag_minus,
        ):
            for pos_ in f(pos, color):
                yield pos_

    def _get_changed_positions_along_xaxis_plus(self, pos, color):
        for x in range(pos[1] + 1, MAX_X):
            p = (pos[0], x)
            if self.get(p) is None:
                break
            elif self.get(p) == color and x - pos[1] >= 2:
                return zip(repeat(pos[0]), range(pos[1] + 1, x))
            elif self.get(p) == color and x == pos[1] + 1:
                break
            else:
                continue
        return {}

    def _get_changed_positions_along_xaxis_minus(self, pos, color):
        for x in reversed(range(0, pos[1])):
            p = (pos[0], x)
            if self.get(p) is None:
                break
            elif self.get(p) == color and pos[1] - x >= 2:
                return zip(repeat(pos[0]), range(x + 1, pos[1]))
            elif self.get(p) == color and pos[1] - x == 1:
                break
            else:
                continue
        return {}

    def _get_changed_positions_along_yaxis_plus(self, pos, color):
        for y in range(pos[0] + 1, MAX_Y):
            p = (y, pos[1])
            if self.get(p) is None:
                break
            elif self.get(p) == color and y - pos[0] >= 2:
                return zip(range(pos[0] + 1, y), repeat(pos[1]))
            elif self.get(p) == color and y == pos[0] + 1:
                break
            else:
                continue
        return {}

    def _get_changed_positions_along_yaxis_minus(self, pos, color):
        for y in reversed(range(0, pos[0])):
            p = (y, pos[1])
            if self.get(p) is None:
                break
            elif self.get(p) == color and pos[0] - y >= 2:
                return zip(range(y + 1, pos[0]), repeat(pos[1]))
            elif self.get(p) == color and pos[0] - y == 1:
                break
            else:
                continue
        return {}

    def _get_changed_positions_along_positive_diag_plus(self, pos, color):
        for p in zip(reversed(range(0, pos[0])), range(pos[1] + 1, MAX_X)):
            if self.get(p) is None:
                break
            elif self.get(p) == color and p[1] - pos[1] >= 2:
                return zip(range(p[0] + 1, pos[0]), reversed(range(pos[1] + 1, p[1])))
            elif self.get(p) == color and p[1] - pos[1] == 1:
                break
            else:
                continue
        return {}

    def _get_changed_positions_along_positive_diag_minus(self, pos, color):
        for p in zip(range(pos[0] + 1, MAX_Y), reversed(range(0, pos[1]))):
            if self.get(p) is None:
                break
            elif self.get(p) == color and p[0] - pos[0] >= 2:
                return zip(range(pos[0] + 1, p[0]), reversed(range(p[1] + 1, pos[1])))
            elif self.get(p) == color and p[0] - pos[0] == 1:
                break
            else:
                continue
        return {}

    def _get_changed_positions_along_negative_diag_plus(self, pos, color):
        for p in zip(reversed(range(0, pos[0])), reversed(range(0, pos[1]))):
            if self.get(p) is None:
                break
            elif self.get(p) == color and pos[0] - p[0] >= 2:
                return zip(range(p[0] + 1, pos[0]), range(p[1] + 1, pos[1]))
            elif self.get(p) == color and pos[0] - p[0] == 1:
                break
            else:
                continue
        return {}

    def _get_changed_positions_along_negative_diag_minus(self, pos, color):
        for p in zip(range(pos[0] + 1, MAX_Y), range(pos[1] + 1, MAX_X)):
            if self.get(p) is None:
                break
            elif self.get(p) == color and p[0] - pos[0] >= 2:
                return zip(range(pos[0] + 1, p[0]), range(pos[1] + 1, p[1]))
            elif self.get(p) == color and p[0] - pos[0] == 1:
                break
            else:
                continue
        return {}

    def reset(self):
        while self.histories:
            self.rollback()

    @staticmethod
    def from_str(s):
        s = "".join([c for c in s if c in "WB."])
        cells = {}
        for y in range(8):
            for x in range(8):
                c = s[8 * y + x]
                cells[(y, x)] = {".": None, "W": Color.WHITE, "B": Color.BLACK}[c]

        return Board(cells)

    def get(self, pos: Position):
        return self.cells[pos]

    def __eq__(self, other):
        return self.cells == other.cells

    def __str__(self):
        s = ""
        for y in range(MAX_Y):
            for x in range(MAX_X):
                s += "." if self.get((y, x)) is None else str(self.get((y, x)))
            s += "\n"
        return s


class NotAbleToPutException(Exception):
    pass


class OthelloBoardFilledException(Exception):
    pass


@dataclass
class User:
    color: Color
    name: str
    board: Board

    def get_next_put_position(self) -> Position:
        pass

    def put(self, pos=None) -> Optional[PutHistory]:
        if not self.exists_puttable_position():
            return None

        if pos is None:
            pos = self.get_next_put_position()

        history = self.board.put(pos, self.color)
        if history is None:
            msg = None
            if pos[0] < 0 or pos[0] >= 8 or pos[1] < 0 or pos[1] >= 8:
                msg = "Invalid position"
            else:
                msg = "There's no cell that you take"
            raise NotAbleToPutException(msg)
        else:
            return history

    def puttable(self, pos: Position) -> bool:
        return self.board.puttable(pos, self.color)

    def get_puttable_positions(self) -> Iterable[Position]:
        return self.board.get_puttable_positions(self.color)

    def exists_puttable_position(self) -> bool:
        return self.board.exists_puttable_position(self.color)


@dataclass
class Game:
    board: Board
    users: Tuple[User, User]
    turn_count: int = 0

    def advance_turn(self) -> Optional[PutHistory]:
        history = self.now_user.put()
        self.turn_count += 1

        return history

    @property
    def now_user(self):
        return self.users[self.turn_count % 2]

    @property
    def winner(self) -> Optional[User]:
        if self.is_finished:
            number_of_cells0 = self.board.count(self.users[0].color)
            number_of_cells1 = self.board.count(self.users[1].color)

            if number_of_cells0 < number_of_cells1:
                return self.users[1]
            elif number_of_cells1 < number_of_cells0:
                return self.users[0]
            else:
                return None
        else:
            return None

    @property
    def scores(self):
        return (self.board.count(Color.BLACK), self.board.count(Color.WHITE))

    @property
    def is_finished(self) -> bool:
        return self.board.is_finished
