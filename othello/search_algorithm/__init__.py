#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dataclasses import dataclass
from typing import Tuple

from othello.entities import Board, Color, Position
from othello.evaluate_function import EvaluateFunction


@dataclass
class SearchAlgorithm:
    evaluate_function: EvaluateFunction

    def run(self, depth: int, board: Board, color: Color) -> Tuple[Position, int]:
        pass
