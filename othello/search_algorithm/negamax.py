#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import List, Tuple

from othello.const import MAX_SCORE, MIN_SCORE
from othello.entities import Position
from othello.search_algorithm import SearchAlgorithm


class NegaMax(SearchAlgorithm):
    def run(self, depth, board, color) -> Tuple[Position, int]:
        hands, score = self._negamax(
            depth, MIN_SCORE, MAX_SCORE, board=board, color=color, my_color=color
        )
        return hands[0], score

    def _negamax(
        self, depth, alpha, beta, board, color, my_color
    ) -> Tuple[List[Position], int]:
        if depth == 0 or board.is_finished:
            return [], self.evaluate_function(board, color)

        positions = board.get_puttable_positions(color)

        max_score = MIN_SCORE if color == my_color else MAX_SCORE
        max_hands = None
        for pos in positions:
            board.put(pos, color)
            res = self._negamax(
                depth=depth - 1,
                alpha=-beta,
                beta=-alpha,
                board=board,
                color=-color,
                my_color=my_color,
            )
            hands = res[0]
            score = -res[1]
            board.rollback()
            if score >= beta:
                return [pos] + hands, score

            if max_hands is None or score > max_score:
                max_score = score
                max_hands = [pos] + hands
                alpha = max(alpha, max_score)

        if max_hands is None:
            return [], max_score
        else:
            return max_hands, max_score
