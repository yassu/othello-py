#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import List, Tuple

from othello.const import MAX_SCORE, MIN_SCORE
from othello.entities import Position
from othello.search_algorithm import SearchAlgorithm


class AlphaBeta(SearchAlgorithm):
    def run(self, depth, board, color) -> Tuple[Position, int]:
        hands, score = self._get_max_score_position(
            depth, alpha=MIN_SCORE, beta=MAX_SCORE, board=board, color=color
        )
        return hands[0], score

    def _get_max_score_position(
        self, depth, alpha, beta, board, color
    ) -> Tuple[List[Position], int]:
        if depth == 0 or board.is_finished:
            return [], self.evaluate_function(board, color)

        positions = board.get_puttable_positions(color)

        max_score = MIN_SCORE
        max_hands = None
        for pos in positions:
            board.put(pos, color)
            hands, score = self._get_min_score_position(
                depth=depth - 1, alpha=alpha, beta=beta, board=board, color=color
            )
            board.rollback()
            if score >= beta:
                return [pos] + hands, score

            if max_hands is None or score > max_score:
                max_score = score
                max_hands = [pos] + hands
                alpha = max(alpha, max_score)

        if max_hands is None:
            return [], max_score
        else:
            return max_hands, max_score

    def _get_min_score_position(
        self, depth, alpha, beta, board, color
    ) -> Tuple[List[Position], int]:
        if depth == 0 or board.is_finished:
            return [], self.evaluate_function(board, color)

        positions = board.get_puttable_positions(-color)

        min_score = MIN_SCORE
        min_hands = None
        for pos in positions:
            board.put(pos, -color)
            hands, score = self._get_max_score_position(
                depth=depth - 1, alpha=alpha, beta=beta, board=board, color=color
            )
            board.rollback()
            if score <= alpha:
                return [pos] + hands, score

            if min_hands is None or score < min_score:
                min_score = score
                min_hands = [pos] + hands
                beta = min(beta, min_score)

        if min_hands is None:
            return [], min_score
        else:
            return min_hands, min_score
