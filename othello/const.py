#!/usr/bin/env python
# -*- coding: utf-8 -*-

MAX_SCORE = 10 ** 8
MIN_SCORE = -MAX_SCORE
