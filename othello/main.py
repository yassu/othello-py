#!/usr/bin/env python
# -*- coding: utf-8 -*-

from optparse import OptionParser

import othello.mains.ci_main as ci_main
import othello.mains.gui_main as gui_main
from othello import VERSION
from othello.users import get_user_cls_from_str
from othello.users.manual_user import ManualUser


def get_option_parser():
    usage = "othello [options]"
    parser = OptionParser(usage=usage, version=VERSION)
    # conf opts
    parser.add_option(
        "-c",
        "--cui",
        action="store_true",
        default=False,
        dest="with_cui",
        help="run with cui",
    )
    parser.add_option(
        "--only-auto",
        action="store_true",
        default=False,
        dest="only_auto",
        help="run by only auto user",
    )
    parser.add_option("-e", "--enemy-logic", default="random", dest="enemy_logic")
    parser.add_option("-m", "--my-logic", default="random", dest="my_logic")
    return parser


def main():
    opts, _ = get_option_parser().parse_args()

    if opts.with_cui:
        my_cls = get_user_cls_from_str(opts.my_logic)
        enemy_cls = get_user_cls_from_str(opts.enemy_logic)
        ci_main.main(my_cls=my_cls, enemy_cls=my_cls)
    else:
        my_cls = get_user_cls_from_str(opts.my_logic) if opts.only_auto else ManualUser
        enemy_cls = get_user_cls_from_str(opts.enemy_logic)
        gui_main.main(my_cls=my_cls, enemy_cls=enemy_cls)


if __name__ == "__main__":
    main()
