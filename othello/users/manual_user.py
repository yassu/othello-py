#!/usr/bin/env python
# -*- coding: utf-8 -*-

from othello.entities import Position, User


class ManualUser(User):
    """
    GUIのユーザーを表すダミーのクラス
    BoardFrameからself.posの値の更新, 取得を行う
    """

    def __init__(self, color, name, board):
        super().__init__(color, name, board)

        self.pos = None

    def get_next_put_position(self) -> Position:
        return self.pos
