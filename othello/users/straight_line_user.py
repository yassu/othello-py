#!/usr/bin/env python
# -*- coding: utf-8 -*-

from random import shuffle

from othello.entities import Position, User


class StraightLineUser(User):
    """
    最も大量のマスが取れる場所に打つユーザー
    """

    def get_next_put_position(self) -> Position:
        positions = list(self.get_puttable_positions())
        shuffle(positions)
        return max(
            positions,
            key=lambda p: len(set(self.board.get_changed_positions(p, self.color))),
        )
