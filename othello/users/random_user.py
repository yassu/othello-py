#!/usr/bin/env python
# -*- coding: utf-8 -*-

from random import choice

from othello.entities import Position, User


class RandomUser(User):
    def get_next_put_position(self) -> Position:
        return choice(list(self.get_puttable_positions()))
