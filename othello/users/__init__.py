#!/usr/bin/env python
# -*- coding: utf-8 -*-
from othello.evaluate_function import EvaluateFunction
from othello.evaluate_function.number_of_hands_score_job import NumberOfHandsScoreJob
from othello.evaluate_function.straight_line_score_job import StraightLineScoreJob
from othello.search_algorithm.alpha_beta import AlphaBeta
from othello.search_algorithm.mini_max import MiniMax
from othello.search_algorithm.negamax import NegaMax

from .random_user import RandomUser
from .researcher import get_researcher
from .straight_line_user import StraightLineUser


class InvalidUserStringException(Exception):
    pass


def get_user_cls_from_str(s: str):
    s = s.lower()

    if s in SIMPLE_USERS:
        return SIMPLE_USERS[s]

    items = [item for item in s.split("::") if item != ""]
    if len(items) >= 1:
        f = EvaluateFunction([StraightLineScoreJob()], [1])
        algorithm = DEFAULT_ALGORITHM
        depth = DEFAULT_RESEARCH_DEPTH
        if len(items) >= 1:
            if items[0] in ALGORITHMS:
                algorithm = ALGORITHMS[items[0]]
            else:
                raise InvalidUserStringException(
                    f"{items[0]} algorithm is not defiled."
                )
        if len(items) >= 2:
            if items[1].isdigit():
                depth = int(items[1])
            else:
                raise InvalidUserStringException(f"depth should be integer.")
        cls = get_researcher(algorithm(f), depth)
        cls.algorithm = algorithm, depth
        return cls

    raise InvalidUserStringException("Invalid logic String")


def _get_mine_user_cls():
    f = EvaluateFunction([StraightLineScoreJob(), NumberOfHandsScoreJob()], [3, 1])
    return get_researcher(AlphaBeta(f), 5)


SIMPLE_USERS = {
    "random": RandomUser,
    "straight": StraightLineUser,
    "mine": _get_mine_user_cls(),
}
ALGORITHMS = {"minimax": MiniMax, "alphabeta": AlphaBeta, "negamax": NegaMax}

DEFAULT_ALGORITHM = MiniMax
DEFAULT_RESEARCH_DEPTH = 3
