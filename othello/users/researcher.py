#!/usr/bin/env python
# -*- coding: utf-8 -*-

from othello.entities import Position, User


class AbstractResearcher(User):
    pass


def get_researcher(search_algorithm, depth):
    class Researcher(AbstractResearcher):
        def get_next_put_position(self) -> Position:
            pos, _ = search_algorithm.run(
                depth=depth, board=self.board, color=self.color
            )
            return pos

    return Researcher
