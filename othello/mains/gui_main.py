#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tkinter as tk
from random import choice
from tkinter import messagebox

from othello.entities import Board, Color, Game
from othello.users.manual_user import ManualUser

CELL_COLORS = {None: "#000fff000", Color.BLACK: "#000", Color.WHITE: "#fff"}
CELL_HEIGHT, CELL_WIDTH = 2, 4


class CellGui(tk.Label):
    def __init__(self, cell, pos, root=None):
        super().__init__(
            root, height=CELL_HEIGHT, width=CELL_WIDTH, background=CELL_COLORS[cell]
        )
        self.cell = cell
        self.pos = pos
        self.board_frame = root


class BoardFrame(tk.Frame):
    def __init__(self, my_cls, enemy_cls, root=None):
        super().__init__(root, width=100, height=200, background="#000")
        self.board = Board()
        is_first = choice([True, False])
        user_clses = (my_cls, enemy_cls) if is_first else (enemy_cls, my_cls)
        if is_first:
            messagebox.showinfo("", "あなたの先行です")
        else:
            messagebox.showinfo("", "あなたの後攻です")
        self.user1 = user_clses[0](color=Color.BLACK, name="User1", board=self.board)
        self.user2 = user_clses[1](color=Color.WHITE, name="User2", board=self.board)
        self.game = Game(board=self.board, users=(self.user1, self.user2))

        self.update_cells()

    def clicked(self, event):
        pos = event.widget.pos
        if isinstance(self.game.now_user, ManualUser):
            self.game.now_user.pos = pos
            if not self.game.now_user.puttable(pos):
                messagebox.showerror("", "その場所にセルを置くことはできません")
                return

        history = self.game.advance_turn()
        self.update_cells()

        print(self.game.board)
        print(history)

        winner = self.game.winner
        if self.game.is_finished:
            msg = None
            if winner is None:
                msg = "Draw ({game.scores[0]} vs {game.scores[1]})"
            else:
                msg = (
                    f"{winner.name}({winner.color}) is winner "
                    f"({self.game.scores[0]} vs {self.game.scores[1]})."
                )
            messagebox.showinfo("", msg)
        else:
            while not self.game.now_user.exists_puttable_position():
                messagebox.showinfo("", winner.name + "には置けるマスがないのでパスします")
                self.game.advance_turn()

    def update_cells(self):
        self.gcells = {}
        for y in range(0, 8):
            for x in range(0, 8):
                self.gcells[(y, x)] = CellGui(self.board.get((y, x)), (y, x), self)
                self.gcells[(y, x)].grid(row=y, column=x, padx=1, pady=1)
                self.gcells[(y, x)].bind("<1>", self.clicked)


class Application(tk.Frame):
    def __init__(self, my_cls, enemy_cls, master=None):
        super().__init__(master)
        self.pack()
        self.create_widgets(my_cls, enemy_cls)

    def create_widgets(self, my_cls, enemy_cls):
        self.board_frame = BoardFrame(my_cls=my_cls, enemy_cls=enemy_cls, root=self)
        self.board_frame.pack()


def main(my_cls, enemy_cls):
    root = tk.Tk()
    root.title("Othello")
    app = Application(my_cls=my_cls, enemy_cls=enemy_cls, master=root)
    app.mainloop()
