#!/usr/bin/env python
# -*- coding: utf-8 -*-

from othello.entities import Board, Color, Game


def main(my_cls, enemy_cls):
    board = Board()
    user1 = my_cls(color=Color.BLACK, name="User1", board=board)
    user2 = enemy_cls(color=Color.WHITE, name="User2", board=board)
    game = Game(board=board, users=(user1, user2))

    while not game.is_finished:
        print("count", game.turn_count)
        print(game.board)
        game.advance_turn()

    print(game.board)

    winner = game.winner
    if winner is None:
        print("Draw ({game.scores[0]} vs {game.scores[1]})")
    else:
        print(
            f"{winner.name}({winner.color}) is winner "
            f"({game.scores[0]} vs {game.scores[1]})."
        )


if __name__ == "__main__":
    main()
