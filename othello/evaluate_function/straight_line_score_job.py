#!/usr/bin/env python
# -*- coding: utf-8 -*-

from othello.entities import Board, Color
from othello.evaluate_function import EvaluateFunctionScoreJob


class StraightLineScoreJob(EvaluateFunctionScoreJob):
    def __call__(self, board: Board, color: Color) -> int:
        return board.count(color) - board.count(-color)
