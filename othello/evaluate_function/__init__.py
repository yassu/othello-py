#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dataclasses import dataclass
from typing import List

from othello.entities import Board, Color


class EvaluateFunctionScoreJob:
    def __call__(self, board, color) -> int:
        pass


@dataclass
class EvaluateFunction:
    score_jobs: List[EvaluateFunctionScoreJob]
    weights: List[int]

    def __call__(self, board: Board, color: Color) -> int:
        score = 0
        for job, weight in zip(self.score_jobs, self.weights):
            score += weight * job(board, color)

        return score
