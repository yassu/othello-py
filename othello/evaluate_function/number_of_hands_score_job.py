#!/usr/bin/env python
# -*- coding: utf-8 -*-

from othello.entities import Board, Color
from othello.evaluate_function import EvaluateFunctionScoreJob


class NumberOfHandsScoreJob(EvaluateFunctionScoreJob):
    def __call__(self, board: Board, color: Color) -> int:
        return len(set(board.get_puttable_positions(color))) - len(
            set(board.get_puttable_positions(-color))
        )
