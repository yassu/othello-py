#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dataclasses import dataclass
from optparse import OptionParser

from prettytable import PrettyTable
from tqdm import tqdm

from othello import VERSION
from othello.entities import Board, Color, Game
from othello.users import get_user_cls_from_str


@dataclass
class Stats:
    user1_name: str
    user2_name: str
    number_of_user1: int = 0
    number_of_user2: int = 0
    win_count_user1: int = 0
    win_count_user2: int = 0
    draw_count: int = 0
    user1_elapsed_time = 0
    user2_elapsed_time = 0

    def update(self, game):
        self.number_of_user1 += game.board.count(Color.BLACK)
        self.number_of_user2 += game.board.count(Color.WHITE)

        if game.winner is None:
            self.draw_count += 1
        elif game.winner.color == Color.BLACK:
            self.win_count_user1 += 1
        elif game.winner.color == Color.WHITE:
            self.win_count_user2 += 1

    def __str__(self):
        table = PrettyTable(header=False)
        table.align = "l"
        table.add_row([f"{self.user1_name}の勝利回数", self.win_count_user1])
        table.add_row([f"{self.user2_name}の勝利回数", self.win_count_user2])
        table.add_row(["引き分けの回数", self.draw_count])
        table.add_row([f"{self.user1_name}が取ったセルの数", self.number_of_user1])
        table.add_row([f"{self.user2_name}が取ったセルの数", self.number_of_user2])
        return table.get_string()


def run_computation(user1_cls, user2_cls, count):
    board = Board()

    board = Board()

    user1 = user1_cls(color=Color.BLACK, name="User1", board=board)
    user2 = user2_cls(color=Color.WHITE, name="User2", board=board)
    stats = Stats(user1.name, user2.name)

    for _ in tqdm(range(count)):
        game = Game(board=board, users=(user1, user2))
        while not game.is_finished:
            game.advance_turn()
        stats.update(game)

        board.reset()

    print(stats)


def get_option_parser():
    usage = "othello-stats [option]"
    parser = OptionParser(usage, version=VERSION)
    parser.add_option(
        "-c", "--count", type=int, default=10, dest="count", help="何回勝負するか"
    )
    parser.add_option(
        "-1",
        "--user1-algorithm",
        default="random",
        dest="user1_algo",
        help="User1の使用するアルゴリズム",
    )
    parser.add_option(
        "-2",
        "--user2-algorithm",
        default="random",
        dest="user2_algo",
        help="User2の使用するアルゴリズム",
    )
    return parser


def main():
    opts, _ = get_option_parser().parse_args()
    user1_cls = get_user_cls_from_str(opts.user1_algo)
    user2_cls = get_user_cls_from_str(opts.user2_algo)
    run_computation(user1_cls=user1_cls, user2_cls=user2_cls, count=opts.count)


if __name__ == "__main__":
    main()
