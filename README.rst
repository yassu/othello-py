==============
othello.py
==============

オセロを題材に探索法のお勉強をするためのプロジェクト.

othelloをGUIで遊ぶためのothelloコマンドとAI同士で戦わせて統計を見るためのothello-statコマンドが実装されている.

Usage of othello command
==========================

::

 Usage: othello [options]

  Options:
    --version             show program's version number and exit
    -h, --help            show this help message and exit
    -c, --cui             run with cui
    --only-auto           run by only auto user
    -e ENEMY_LOGIC, --enemy-logic=ENEMY_LOGIC
    -m MY_LOGIC, --my-logic=MY_LOGIC

Usage of othello-stat command
===============================

::

 Usage: othello-stats [option]

 Options:
   --version             show program's version number and exit
   -h, --help            show this help message and exit
   -c COUNT, --count=COUNT 何回勝負するか
   -1 USER1_ALGO, --user1-algorithm=USER1_ALGO User1の使用するアルゴリズム
   -2 USER2_ALGO, --user2-algorithm=USER2_ALGO User2の使用するアルゴリズム

アルゴリズムの文字列の形式
=========================================

ロジックとしては {探索アルゴリズム名}[::{探索の深さ}][::{評価関数名1}:{評価関数1の重み}::{評価関数名2}:{評価関数2の重み}::...]

という文字列で渡すことができる(まだできてない).

Author
=======

Yassu <mathyassu@gmail.com>

License
========

Apache License 2.0
