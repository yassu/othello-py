# #!/usr/bin/env python
# # -*- coding: utf-8 -*-
#
# from othello.entities import Board, Color
# from othello.evaluate_function.straight_line_evaluate_function import (
#     StraightLineEvaluateFunction,
# )
#
#
# def test_StraightLineEvaluateFunction_call():
#     board = Board()
#     f = StraightLineEvaluateFunction()
#     assert f(board, Color.BLACK) == f(board, Color.WHITE)
#
#
# def test_StraightLineEvaluateFunction_call2():
#     board = Board()
#     f = StraightLineEvaluateFunction()
#     old_score = f(board, Color.BLACK)
#     assert board.put((2, 3), Color.BLACK)
#     assert f(board, Color.BLACK) > old_score
