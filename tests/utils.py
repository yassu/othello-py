#!/usr/bin/env python
# -*- coding: utf-8 -*-

from copy import deepcopy

from othello.entities import Board, Color, Game
from othello.evaluate_function import EvaluateFunction
from othello.evaluate_function.straight_line_score_job import StraightLineScoreJob


def assert_play_games_test(user1_cls, user2_cls, count=10, board=None):
    if board is None:
        board = Board()
    for _ in range(count):
        user1 = user1_cls(color=Color.BLACK, name="", board=board)
        user2 = user2_cls(color=Color.WHITE, name="", board=board)
        game = Game(board=board, users=(user1, user2))

        while not game.is_finished:
            old_board = deepcopy(board)
            history = game.advance_turn()
            if history is not None:
                assert game.board != old_board

        board.reset()

        return game


StraightLineEvaluateFunction = EvaluateFunction([StraightLineScoreJob()], [1])
