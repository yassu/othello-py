#!/usr/bin/env python
# -*- coding: utf-8 -*-

from copy import deepcopy
from itertools import product
from random import choice
from unittest.mock import Mock

import pytest

from othello.entities import Board, Color, Game, NotAbleToPutException, PutHistory, User
from othello.users.random_user import RandomUser


def test_Color_str():
    assert Color.BLACK.value == 1
    assert Color.WHITE.value == -1


def test_Color_neg():
    assert -Color.BLACK == Color.WHITE


def test_Board_is_filled1():
    """
    初期状態
    """
    board = Board()
    assert board.is_filled is False


def test_Board_is_filled2():
    """
    全てのマスが埋め尽くされた状態
    """
    cells = {
        (y, x): choice([Color.BLACK, Color.WHITE])
        for (y, x) in product(range(8), range(8))
    }
    board = Board(cells=cells)
    assert board.is_filled is True


def test_Board_count():
    """
    初期状態から一手だけ打った後
    """
    board = Board()
    assert board.put((2, 3), Color.BLACK)
    assert board.count(Color.BLACK) == 4
    assert board.count(Color.WHITE) == 1


def test_Board_rollback():
    board = Board()
    init_board = deepcopy(board)
    board.put((2, 3), Color.BLACK)
    board.put((2, 2), Color.WHITE)
    assert board.get((3, 3)) == Color.WHITE
    board.rollback()
    assert board.get((3, 3)) == Color.BLACK
    board.rollback()
    board == init_board


def test_Board_put():
    """
    セルが置かれていて おけない場合
    """
    board = Board()
    assert board.put((3, 3), Color.BLACK) is None


def test_Board_put2():
    """
    置こうとしている場所の周りにセルがなくて置けない場合
    """
    board = Board()
    assert board.put((0, 0), Color.BLACK) is None


def test_Board_put3():
    """
    1つだけひっくり返る場合
    """
    board = Board()
    assert board.put((3, 2), Color.BLACK)
    assert board.cells[(3, 2)] == Color.BLACK
    assert board.cells[(3, 3)] == Color.BLACK


def test_Board_put4():
    """
    2つひっくり返る場合
    """
    board = Board()
    assert board.put((5, 4), Color.BLACK)
    assert board.put((3, 5), Color.WHITE)
    assert board.put((6, 4), Color.WHITE)
    assert board.get((4, 4)) == Color.WHITE
    assert board.get((5, 4)) == Color.WHITE


def test_Board_put5():
    """
    一度おかしな動きになったオセロ
    """
    board = Board()
    assert board.put((3, 2), Color.BLACK)
    assert board.put((2, 4), Color.WHITE)
    assert board.put((4, 5), Color.BLACK)
    assert board.put((5, 4), Color.WHITE)
    assert board.put((1, 5), Color.BLACK)
    assert board.put((4, 2), Color.WHITE)
    assert board.put((6, 3), Color.BLACK)
    assert board.put((0, 6), Color.WHITE) == PutHistory(
        color=Color.WHITE,
        put_position=(0, 6),
        changed_positions={(1, 5), (2, 4), (3, 3)},
    )


def test_Board_put6():
    """
    一度おかしな動きになったオセロ
    """
    board = Board.from_str(
        """
    BBW..B..
    .BBWBBWW
    .WWBWBWW
    .WWWWBBW
    .W.WWBBW
    ...WWWBW
    ...BWBBB
    ....WW.W
    """
    )
    assert board.put((4, 2), Color.BLACK) == PutHistory(
        color=Color.BLACK,
        put_position=(4, 2),
        changed_positions={(2, 2), (3, 2), (4, 3), (4, 4), (3, 3), (2, 4)},
    )
    # (3, 3)と(2, 4)が問題


def test_Board_get_changed_positions_along_xaxis_plus():
    board = Board()
    assert set(board._get_changed_positions_along_xaxis_plus((3, 2), Color.BLACK)) == {
        (3, 3)
    }


def test_Board_get_changed_positions_along_xaxis_minus():
    board = Board()
    assert set(board._get_changed_positions_along_xaxis_minus((4, 5), Color.BLACK)) == {
        (4, 4)
    }


def test_Board_get_changed_positions_along_yaxis_plus():
    board = Board()
    assert set(board._get_changed_positions_along_yaxis_plus((2, 3), Color.BLACK)) == {
        (3, 3)
    }


def test_Board_get_changed_positions_along_yaxis_minus():
    board = Board()
    assert set(board._get_changed_positions_along_yaxis_minus((5, 4), Color.BLACK)) == {
        (4, 4)
    }


def test_Board_get_changed_positions_along_positive_diag_plus():
    board = Board()
    assert board.put((5, 3), Color.WHITE)
    assert set(
        board._get_changed_positions_along_positive_diag_plus((5, 2), Color.BLACK)
    ) == {(4, 3)}


def test_Board_get_changed_positions_along_positive_diag_minus():
    board = Board()
    assert board.put((2, 4), Color.WHITE)
    assert set(
        board._get_changed_positions_along_positive_diag_minus((2, 5), Color.BLACK)
    ) == {(3, 4)}


def test_Board_get_changed_positions_along_negative_diag_plus():
    board = Board()
    assert board.put((5, 4), Color.BLACK)
    assert set(
        board._get_changed_positions_along_negative_diag_plus((5, 5), Color.WHITE)
    ) == {(4, 4)}


def test_Board_get_changed_positions_along_negative_diag_minus():
    board = Board()
    assert board.put((2, 3), Color.BLACK)
    assert set(
        board._get_changed_positions_along_negative_diag_minus((2, 2), Color.WHITE)
    ) == {(3, 3)}


def test_Board_reset():
    board = Board()
    init_board = deepcopy(board)
    board.put((2, 3), Color.BLACK)
    board.put((2, 2), Color.WHITE)
    board.reset()
    assert board == init_board


def test_Board_from_str():
    board = Board()
    assert board.put((2, 3), Color.BLACK)
    assert Board.from_str(str(board)) == board


def test_Board_str():
    board = Board()
    board.put((3, 2), Color.BLACK)
    assert str(board) == (
        "........\n"
        "........\n"
        "........\n"
        "..BBB...\n"
        "...BW...\n"
        "........\n"
        "........\n"
        "........\n"
    )


def test_User_put():
    """
    初期状態からお互いに一手ずつ打ち合う
    """
    board = Board()
    user1 = User(color=Color.BLACK, name="yassu", board=board)
    user2 = User(color=Color.WHITE, name="yassu2", board=board)

    user1.get_next_put_position = Mock(return_value=(2, 3))
    user2.get_next_put_position = Mock(return_value=(2, 4))

    user1.put()
    user2.put()

    assert board.count(Color.BLACK) == 3
    assert board.count(Color.WHITE) == 3
    assert board.get((3, 3)) == Color.BLACK
    assert board.get((3, 4)) == Color.WHITE


def test_User_put2():
    """
    置けない場所にputするとExceptionが起こること
    """
    board = Board()
    user = User(color=Color.BLACK, name="yassu", board=board)
    user.get_next_put_position = Mock(return_value=(2, 4))
    with pytest.raises(NotAbleToPutException):
        user.put()


def test_User_put3():
    """
    置ける場所がない場合にputしても boardは変化しないこと
    """
    board = Board()
    board.cells[(3, 4)] = Color.WHITE
    board.cells[(4, 3)] = Color.WHITE
    user = User(color=Color.BLACK, name="yassu", board=board)
    board1 = deepcopy(board)
    user.put()
    assert board == board1


def test_User_puttable():
    """
    初期状態で打てる場合
    """
    board = Board()
    user = User(color=Color.BLACK, name="yassu", board=board)
    assert user.puttable((2, 3)) is True


def test_User_puttable2():
    """
    初期状態で打てない場合
    """
    board = Board()
    user = User(color=Color.BLACK, name="yassu", board=board)
    assert user.puttable((2, 4)) is False


def test_User_puttable3():
    """
    置いてある場所には打てないこと
    """
    board = Board()
    user1 = User(color=Color.BLACK, name="yassu", board=board)
    user2 = User(color=Color.WHITE, name="yassu", board=board)
    user1.put((4, 5))
    print(board)
    user2.put((5, 5))
    print(board)
    assert user1.puttable((4, 3)) is False


def test_User_get_puttable_positions():
    """
    初期状態での打てる場所のテスト
    """
    board = Board()
    user1 = User(color=Color.BLACK, name="yassu", board=board)
    user2 = User(color=Color.WHITE, name="yassu2", board=board)
    assert set(user1.get_puttable_positions()) == {(2, 3), (3, 2), (4, 5), (5, 4)}
    assert set(user2.get_puttable_positions()) == {(2, 4), (3, 5), (4, 2), (5, 3)}


def test_User_exists_puttable_position():
    """
    初期状態では 置ける場所はあること
    """
    board = Board()
    user = User(color=Color.BLACK, name="yassu", board=board)
    assert user.exists_puttable_position() is True


def test_User_puttable_all_positions2():
    """
    全て白では打てる場所がないこと
    """
    cells = {
        (y, x): choice([None, Color.WHITE]) for (y, x) in product(range(8), range(8))
    }
    board = Board(cells)
    user = User(color=Color.BLACK, name="yassu", board=board)
    user2 = User(color=Color.WHITE, name="yassu2", board=board)
    assert user.exists_puttable_position() is False
    assert user2.exists_puttable_position() is False


def test_Game_advance_turn():
    board = Board()
    user1 = User(color=Color.BLACK, name="", board=board)
    user2 = User(color=Color.WHITE, name="", board=board)
    game = Game(board=board, users=(user1, user2))

    user1.get_next_put_position = Mock(return_value=(2, 3))
    user2.get_next_put_position = Mock(return_value=(2, 4))

    assert game.turn_count == 0
    game.advance_turn()
    assert game.turn_count == 1
    assert game.board.get((3, 3)) == Color.BLACK
    game.advance_turn()
    assert game.turn_count == 2
    assert game.board.get((3, 4)) == Color.WHITE


def test_Game_advance_turn2():
    """
    RandomUserでゲームをして, 常に返り値のHistoryに打った手が含まれていないこと
    """
    for _ in range(10):
        board = Board()
        user1 = RandomUser(color=Color.BLACK, name="yassu", board=board)
        user2 = RandomUser(color=Color.WHITE, name="aiya", board=board)
        game = Game(board=board, users=(user1, user2))
        if game.now_user.exists_puttable_position():
            history = game.advance_turn()
            assert history.put_position not in history.changed_positions


def test_Game_winner():
    """
    初期状態から一手さした状態ではwinnerは確定しないこと
    """
    board = Board()
    user1 = User(color=Color.BLACK, name="", board=board)
    user2 = User(color=Color.WHITE, name="", board=board)
    game = Game(board=board, users=(user1, user2))

    user1.get_next_put_position = Mock(return_value=(2, 3))
    game.advance_turn()

    assert game.winner is None


def test_Game_winner2():
    """
    63個のセルが黒なら黒の勝ち
    """
    cells = {(y, x): Color.BLACK for (y, x) in product(range(8), range(8))}
    cells[(3, 3)] = Color.WHITE

    board = Board(cells)
    user1 = User(color=Color.BLACK, name="", board=board)
    user2 = User(color=Color.WHITE, name="", board=board)
    game = Game(board=board, users=(user1, user2))

    assert game.winner == user1


def test_Game_winner3():
    """
    白のセルしかなければ 黒の勝ち
    """
    cells = {
        (y, x): choice([Color.WHITE, None]) for (y, x) in product(range(8), range(8))
    }

    board = Board(cells)
    user1 = User(color=Color.BLACK, name="", board=board)
    user2 = User(color=Color.WHITE, name="", board=board)
    game = Game(board=board, users=(user1, user2))

    assert game.winner == user2


def test_Game_is_finished():
    """
    初期状態では終了していないこと
    """
    board = Board()
    user1 = User(color=Color.BLACK, name="", board=board)
    user2 = User(color=Color.WHITE, name="", board=board)
    game = Game(board=board, users=(user1, user2))

    assert game.is_finished is False


def test_Game_is_finished2():
    """
    全てのマスにセルが置かれていたら終了していること
    """
    cells = {
        (y, x): choice([Color.BLACK, Color.WHITE])
        for (y, x) in product(range(8), range(8))
    }
    board = Board(cells)
    user1 = User(color=Color.BLACK, name="", board=board)
    user2 = User(color=Color.WHITE, name="", board=board)
    game = Game(board=board, users=(user1, user2))

    assert game.is_finished is True


def test_Game_is_finished3():
    """
    全てのマスが空か黒なら終了していること
    """
    cells = {
        (y, x): choice([Color.BLACK, None]) for (y, x) in product(range(8), range(8))
    }
    board = Board(cells)
    user1 = User(color=Color.BLACK, name="", board=board)
    user2 = User(color=Color.WHITE, name="", board=board)
    game = Game(board=board, users=(user1, user2))

    assert game.is_finished is True
