#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from othello.entities import Board
from othello.search_algorithm.mini_max import MiniMax
from othello.users.random_user import RandomUser
from othello.users.researcher import get_researcher
from tests.utils import StraightLineEvaluateFunction, assert_play_games_test


@pytest.mark.slow
def test_MiniMax_user_get_next_put_position():
    board = Board()
    researcher_cls = get_researcher(MiniMax(StraightLineEvaluateFunction), 3)
    assert_play_games_test(researcher_cls, RandomUser, board=board, count=3)
