#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from othello.entities import Board
from othello.search_algorithm.alpha_beta import AlphaBeta
from othello.users.random_user import RandomUser
from othello.users.researcher import get_researcher
from tests.utils import StraightLineEvaluateFunction, assert_play_games_test


@pytest.mark.slow
def test_AlphaBeta_user_get_next_put_position():
    board = Board()
    researcher_cls = get_researcher(AlphaBeta(StraightLineEvaluateFunction), 3)
    assert_play_games_test(researcher_cls, RandomUser, board=board, count=3)


@pytest.mark.slow
def test_AlphaBeta_vs_Minimax_test():
    """
    同じdepth=3だったら minimaxとalphabetaが戦っても常に後手が勝つこと
    """
    board = Board()
    researcher_cls = get_researcher(AlphaBeta(StraightLineEvaluateFunction), 3)
    game = assert_play_games_test(researcher_cls, RandomUser, board=board, count=1)

    return game.winner == game.users[1]
