#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from othello.search_algorithm.mini_max import MiniMax
from othello.users import (
    DEFAULT_RESEARCH_DEPTH,
    InvalidUserStringException,
    RandomUser,
    get_user_cls_from_str,
)
from othello.users.straight_line_user import StraightLineUser


def test_get_user_cls_from_str1():
    assert get_user_cls_from_str("random") == RandomUser
    assert get_user_cls_from_str("straight") == StraightLineUser


def test_get_user_cls_from_str2():
    assert get_user_cls_from_str("Random") == RandomUser


def test_get_user_cls_from_str3():
    user_cls = get_user_cls_from_str("minimax::5")
    assert user_cls.algorithm[0] == MiniMax
    assert user_cls.algorithm[1] == 5


def test_get_user_cls_from_str4():
    user_cls = get_user_cls_from_str("minimax")
    assert user_cls.algorithm[0] == MiniMax
    assert user_cls.algorithm[1] == DEFAULT_RESEARCH_DEPTH


def test_get_user_cls_from_str5():
    """ 空文字が指定された場合 """
    with pytest.raises(InvalidUserStringException):
        get_user_cls_from_str("")


def test_get_user_cls_from_str6():
    """ 間違った文字列がアルゴリズム名として指定された場合 """
    with pytest.raises(InvalidUserStringException):
        get_user_cls_from_str("abc")


def test_get_user_cls_from_str7():
    """ depthとして数値以外が指定された場合 """
    with pytest.raises(InvalidUserStringException):
        get_user_cls_from_str("minimax::0.3")
