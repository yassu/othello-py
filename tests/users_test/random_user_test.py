#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pytest

from othello.users.random_user import RandomUser
from tests.utils import assert_play_games_test


@pytest.mark.slow
def test_random_user_get_next_put_position():
    assert_play_games_test(RandomUser, RandomUser)
