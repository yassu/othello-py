#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pytest

from othello.entities import Board, Color, Game
from othello.users.straight_line_user import StraightLineUser
from tests.utils import assert_play_games_test


@pytest.mark.slow
def test_get_next_put_position():
    assert_play_games_test(StraightLineUser, StraightLineUser)


def test_simple_game():
    board = Board.from_str(
        """
    BWWWWWW.
    BBBBB...
    WWWWW...
    ........
    ........
    ........
    ........
    ........
    """
    )
    user1 = StraightLineUser(color=Color.BLACK, name="", board=board)
    user2 = StraightLineUser(color=Color.WHITE, name="", board=board)
    game = Game(board=board, users=(user1, user2))
    history = game.advance_turn()
    assert history.put_position == (0, 7)
